import { createRouter, createWebHistory } from 'vue-router';
import wwPage from './views/wwPage.vue';

import { initializeData, onPageUnload } from '@/_common/helpers/data';

let router;
const routes = [];


/* wwFront:start */
// eslint-disable-next-line no-undef
window.wwg_designInfo = {"id":"c3077c98-3dcd-45ff-9839-8b7c18f98c7a","homePageId":"d8c99c90-76c6-4000-aef7-c623fe373798","designVersionId":"3671f78f-14c6-4141-8034-1d63e975fcc5","cacheVersion":88,"langs":[{"lang":"en","default":false,"isDefaultPath":false},{"lang":"de","default":true,"isDefaultPath":false}],"baseTag":null,"name":"Dufour","workflows":[],"prerenderDisabled":false,"wewebPreviewURL":null,"pages":[{"id":"d8c99c90-76c6-4000-aef7-c623fe373798","linkId":"8e106f57-c664-4b2a-90e5-97bfc461d9d2","paths":{"en":"home","default":"home"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"b08e02d3-a4c1-45ed-bae5-4d85198e94b9","linkId":"92ab1b8f-a889-43aa-b3f2-569dbed09941","sectionTitle":"Hero"},{"uid":"75657b31-e94d-4e26-90a4-58f928547cf3","linkId":"004b98e4-1392-4c81-8291-b05947ecbf8c","sectionTitle":"Participation"},{"uid":"fcc1c3a1-1543-48be-b88d-039d46098821","linkId":"30d15f8d-b9d8-42db-833d-b43fe1d2a489","sectionTitle":"Grundlagen"},{"uid":"65cc6de0-c7a9-4e30-a1a6-ae9c6770fd22","linkId":"9f5da8d1-92e0-45f6-b8ea-293fa4502366","sectionTitle":"All in One Place"},{"uid":"42a5905f-db93-42a9-bded-2d79d07db3f1","linkId":"c9ede28f-7d08-4c4b-94f9-b9b69aa9352e","sectionTitle":"Image with text - Io"},{"uid":"eefc57e3-e526-4708-9c0e-f567ddb5a651","linkId":"8555d479-0490-4d37-89c3-deb7dadca396","sectionTitle":"Text with image - Io"},{"uid":"92002af3-bf15-472a-ae05-bb5e22c1c3ce","linkId":"658a83d4-4b70-4d0a-a802-ba47946a9132","sectionTitle":"Features"},{"uid":"c73177d1-529b-47c9-93b5-5edabae83a45","linkId":"e8e4f539-5be2-4c4b-8606-81a0e7407e4a","sectionTitle":"Our clients"},{"uid":"882c5bbd-a460-4cc4-9d20-260e05d3e5f9","linkId":"9e7b4ada-0dc7-4702-8398-4614e236a4cf","sectionTitle":"Slider - Callisto"},{"uid":"5d7d0b84-ce24-43e8-b4ef-c2c409f6be28","linkId":"40f90e3a-84c2-4c27-a787-ac700b55298c","sectionTitle":"Pricing - Io - Copy"},{"uid":"15917e0e-ec47-4e69-b390-33d72c6bbdc2","linkId":"d54beeaa-2bd5-407a-88e1-3d49e65b2a17","sectionTitle":"get-started-form - Copy"},{"uid":"bc333f7b-9cda-4891-b8e1-5fe42d6b2f6c","linkId":"f2ce5301-7c88-42be-9977-b4caba4fde4f","sectionTitle":"Brand logos - Callisto"},{"uid":"02006383-c3ae-4de4-8e72-35b7a3f888fd","linkId":"8816ef67-cd4d-4994-955e-889c1774b7ef","sectionTitle":"Article list - Io"},{"uid":"82d9c789-2260-45ac-945d-e4cbed97492f","linkId":"cb5e17ef-fd7a-4184-a0c3-3ba3b27ea678","sectionTitle":"BlueLion"},{"uid":"face27e5-96e7-43ae-9766-f4b9710b3601","linkId":"5b94a113-89f9-47ca-8f77-e6d254a4c12e","sectionTitle":"Footer"}],"title":{"de":"Dføur","en":"Dføur","fr":"Vide | Commencer à partir de zéro"},"meta":{"desc":{"de":"Collaborative GIS Platform of the future","en":"Collaborative GIS Platform of the future"},"keywords":{"de":"spatial planning, urban, real estate, GIS, saas","en":"spatial planning, urban, real estate, GIS, saas"},"__typename":"PageMeta","socialDesc":{},"socialTitle":{},"structuredData":{}},"metaImage":"https://cdn.weweb.io/designs/c3077c98-3dcd-45ff-9839-8b7c18f98c7a/sections/Artboard_1.png?v=1618569224530","favicon":"https://cdn.weweb.io/designs/c3077c98-3dcd-45ff-9839-8b7c18f98c7a/sections/background.png?v=1636189080440"},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_0","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_0","paths":{"de":"recbl5ximzcpyjank","en":"recbl5ximzcpyjank","default":"recbl5ximzcpyjank"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_1","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_1","paths":{"de":"recfx8lg1hvmkqbfo","en":"recfx8lg1hvmkqbfo","default":"recfx8lg1hvmkqbfo"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_2","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_2","paths":{"de":"rechqyccxlbrfvt4s","en":"rechqyccxlbrfvt4s","default":"rechqyccxlbrfvt4s"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_3","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_3","paths":{"de":"recsymbha1kqn4bml","en":"recsymbha1kqn4bml","default":"recsymbha1kqn4bml"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_4","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_4","paths":{"de":"recmj3p2v6gdrg41d","en":"recmj3p2v6gdrg41d","default":"recmj3p2v6gdrg41d"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_5","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_5","paths":{"de":"rec15ept2drphptjz","en":"rec15ept2drphptjz","default":"rec15ept2drphptjz"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_6","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_6","paths":{"de":"recksg0dklmpunc3r","en":"recksg0dklmpunc3r","default":"recksg0dklmpunc3r"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_7","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_7","paths":{"de":"recaakweizohu2ts6","en":"recaakweizohu2ts6","default":"recaakweizohu2ts6"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_8","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_8","paths":{"de":"recglqibyopecgbo4","en":"recglqibyopecgbo4","default":"recglqibyopecgbo4"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_9","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_9","paths":{"de":"rec6zmiutmy7scbzu","en":"rec6zmiutmy7scbzu","default":"rec6zmiutmy7scbzu"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_10","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_10","paths":{"de":"rectojzqlf42elqwe","en":"rectojzqlf42elqwe","default":"rectojzqlf42elqwe"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_11","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_11","paths":{"de":"recp2sdszhjjon10l","en":"recp2sdszhjjon10l","default":"recp2sdszhjjon10l"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_12","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_12","paths":{"de":"recjlldk5t9udxzi5","en":"recjlldk5t9udxzi5","default":"recjlldk5t9udxzi5"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_13","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_13","paths":{"de":"recd1ygbrvasslui6","en":"recd1ygbrvasslui6","default":"recd1ygbrvasslui6"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"04a57aef-0ef9-427a-889e-0303a4c8b564_14","linkId":"04a57aef-0ef9-427a-889e-0303a4c8b564_14","paths":{"de":"recg8912dgwf2qsnj","en":"recg8912dgwf2qsnj","default":"recg8912dgwf2qsnj"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"d8f6e414-f0b8-4220-a218-3bec5147cce0","linkId":"31d02896-cec8-4896-af8a-3ef4742306a8","sectionTitle":"Detail View"}]},{"id":"cb3dea78-44ae-4247-8ff1-b645916568a3","linkId":"cb3dea78-44ae-4247-8ff1-b645916568a3","paths":{"de":"signup","default":"signup"},"langs":["en","de"],"sections":[{"uid":"94906fb2-e71c-4b27-8d21-e468ba97ef8c","linkId":"e5aafc10-c59c-4f26-9bcd-dc21e5c80b57","sectionTitle":"get-started-form"}]},{"id":"2b59a1d7-a616-45bf-9f92-81a9093382e6","linkId":"2b59a1d7-a616-45bf-9f92-81a9093382e6","paths":{"de":"detail","default":"detail"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"c7754642-3b7b-452b-b63d-027041d42219","linkId":"4f3cacd4-6244-46f2-b43d-fd7bc464f4cd","sectionTitle":"Detail View"}]},{"id":"c74dd8ab-9cb6-4de5-b35b-9b24b2d919d4","linkId":"c74dd8ab-9cb6-4de5-b35b-9b24b2d919d4","paths":{"de":"catalogue","default":"catalogue"},"langs":["en","de"],"sections":[{"uid":"b5c5e480-11f1-440e-bd2c-cfc3a5d9c621","linkId":"d7e20c9e-7fd7-4ef5-be5e-504e2da2582a","sectionTitle":"Navigation bar"},{"uid":"9e96f69d-58bc-4c7c-9a97-9762285e82c9","linkId":"731fd653-24aa-4518-ae58-e0b32208991f","sectionTitle":"Erreichbarkeit"},{"uid":"657de156-07ab-4c3c-8d8c-80ddc270df13","linkId":"0fa8188c-2c8b-43fe-8ee8-53c8f9022407","sectionTitle":"Cards"},{"uid":"5f2fe005-ec6b-4c92-8535-7afde76afc31","linkId":"19cb5327-39b1-4293-814a-5c7b778b6d09","sectionTitle":"Blank"}]},{"id":"78f321d0-21bd-432a-a4db-3506fe486511","linkId":"78f321d0-21bd-432a-a4db-3506fe486511","paths":{"de":"demo","default":"demo"},"langs":["en","de"],"sections":[{"uid":"06357135-33a9-48f1-b46d-e15ac5389b58","linkId":"f0adfcba-cf4d-499f-b751-636cc2f494e7","sectionTitle":"Navigation bar"},{"uid":"cd74dbd9-1fff-403d-805f-5fa7917c5b6f","linkId":"233b46b1-85e8-41e0-83ac-d6b99335f7f2","sectionTitle":"About us - Callisto"},{"uid":"8dc655d0-c5c7-4540-b729-80e95704bfa3","linkId":"7c5c55fe-6ba5-486f-ac9b-06fc134559b6","sectionTitle":"Absolute popup"}]}],"plugins":[{"id":"29809245-a5ea-4687-af79-952998abab22","name":"airtable","hasCode":false,"versionId":"eeb794ba-a70e-49e8-a042-35703d53ae69"},{"id":"615d0fde-2084-4ac2-a2a4-5eb7686446ce","name":"fluxRSS","hasCode":false,"versionId":"f7849af7-fbdc-41e5-ad43-011ec859cfb1"},{"id":"2bd1c688-31c5-443e-ae25-59aa5b6431fb","name":"restApi","hasCode":false,"versionId":"71a9bc8c-1b5c-4c18-b218-0089504e1b24"},{"id":"d8a4685d-3cff-4975-87f1-bc8ae8878260","name":"snipcart","hasCode":false,"versionId":"ff3df84d-5205-4ffb-a0ef-ad50818b8e43"}]};
// eslint-disable-next-line no-undef
window.wwg_cacheVersion = 88;
// eslint-disable-next-line no-undef
window.wwg_pluginsSettings = {"29809245-a5ea-4687-af79-952998abab22":{"id":"4097d9b0-eeb9-443d-acc9-b7ba39dcff08","designId":"c3077c98-3dcd-45ff-9839-8b7c18f98c7a","pluginId":"29809245-a5ea-4687-af79-952998abab22","publicData":{}},"615d0fde-2084-4ac2-a2a4-5eb7686446ce":{"id":"da0aaa00-4df2-40ff-9be3-ab07809ec785","designId":"c3077c98-3dcd-45ff-9839-8b7c18f98c7a","pluginId":"615d0fde-2084-4ac2-a2a4-5eb7686446ce","publicData":{}},"2bd1c688-31c5-443e-ae25-59aa5b6431fb":{"id":"dc9ecccf-4fe5-46e6-811f-2d68916e0621","designId":"c3077c98-3dcd-45ff-9839-8b7c18f98c7a","pluginId":"2bd1c688-31c5-443e-ae25-59aa5b6431fb","publicData":{}},"d8a4685d-3cff-4975-87f1-bc8ae8878260":{"id":"3b6c5269-8385-4176-91b3-3470e0873555","designId":"c3077c98-3dcd-45ff-9839-8b7c18f98c7a","pluginId":"d8a4685d-3cff-4975-87f1-bc8ae8878260","publicData":{}}};

const defaultLang = window.wwg_designInfo.langs.find(({ default: isDefault }) => isDefault) || {};

const registerRoute = (page, lang, forcedPath) => {
    const langSlug = !lang.default || lang.isDefaultPath ? `/${lang.lang}` : '';
    let path =
        forcedPath ||
        (page.id === window.wwg_designInfo.homePageId ? '/' : `/${page.paths[lang.lang] || page.paths.default}`);

    //Replace params
    path = path.replace(/{{([\w]+)\|([^/]+)?}}/g, ':$1');

    routes.push({
        path: langSlug + path,
        component: wwPage,
        name: `page-${page.id}-${lang.lang}`,
        meta: {
            pageId: page.id,
        },
        async beforeEnter(to, from, next) {
            if (to.name === from.name) return next();
            //If the app has already been mounted once (hydration of prerendered content done)
            //We set isPrerenderHydration to false so we don't lazy load sections
            if (wwLib.isMounted) wwLib.$store.dispatch('front/setIsPrerenderHydration', false);

            //Set page lang
            wwLib.wwLang.defaultLang = defaultLang.lang;
            wwLib.$store.dispatch('front/setLang', lang.lang);

            try {
                await import(`@/pages/${page.id.split('_')[0]}/components.js`);
                await wwLib.wwWebsiteData.fetchPage(page.id);

                //Scroll to section or on top after page change
                if (to.hash) {
                    const targetElement = document.getElementById(to.hash.replace('#', ''));
                    if (targetElement) targetElement.scrollIntoView();
                } else {
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }

                return next();
            } catch (err) {
                if (err.redirectUrl) {
                    return next({ path: err.redirectUrl || '404' });
                } else {
                    //Any other error: go to target page using window.location
                    window.location = to.fullPath;
                }
            }
        },
    });
};

for (const page of window.wwg_designInfo.pages) {
    for (const lang of window.wwg_designInfo.langs) {
        if (!page.langs.includes(lang.lang)) continue;
        registerRoute(page, lang);
    }
}

if (!window.__WW_IS_PRERENDER__) {
    const page404 = window.wwg_designInfo.pages.find(page => page.paths.default === '404');
    if (page404) {
        for (const lang of window.wwg_designInfo.langs) {
            // Create routes /:lang/:pathMatch(.*)* etc for all langs of the 404 page
            if (!page404.langs.includes(lang.lang)) continue;
            registerRoute(
                page404,
                {
                    default: false,
                    lang: lang.lang,
                },
                '/:pathMatch(.*)*'
            );
        }
        // Create route /:pathMatch(.*)* using default project lang
        registerRoute(page404, { default: true, isDefaultPath: false, lang: defaultLang.lang }, '/:pathMatch(.*)*');
    } else {
        routes.push({
            path: '/:pathMatch(.*)*',
            async beforeEnter() {
                window.location.href = '/404';
            },
        });
    }
}

let routerOptions = {};

if (!window.__WW_IS_PRERENDER__ && window.wwg_designInfo.baseTag && window.wwg_designInfo.baseTag.href) {
    routerOptions = {
        base: window.wwg_designInfo.baseTag.href,
        history: createWebHistory(window.wwg_designInfo.baseTag.href),
        routes,
    };
} else {
    routerOptions = {
        history: createWebHistory(),
        routes,
    };
}

router = createRouter(routerOptions);

if (!window.__WW_IS_PRERENDER__) {
    //Trigger on page unload
    let isFirstNavigation = true;
    router.beforeEach(async (to, _from, next) => {
        if (!isFirstNavigation) await onPageUnload();
        isFirstNavigation = false;
        return next();
    });

    //Init page
    router.afterEach((to, from, failure) => {
        let fromPath = from.path;
        let toPath = to.path;
        if (!fromPath.endsWith('/')) fromPath = fromPath + '/';
        if (!toPath.endsWith('/')) toPath = toPath + '/';
        if (failure || (from.name && toPath === fromPath)) return;
        initializeData(to);
    });
}
/* wwFront:end */

export default router;
