// eslint-disable-next-line no-unused-vars
import app from '@/_front/main.js';
import { defineAsyncComponent } from 'vue';


// eslint-disable-next-line no-undef
import wwobject_fb3e0024_f017_4193_a6a1_bc2eed330d1d from '@/components/wwObjects/wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d/src/wwElement.vue' 
import wwobject_d7904e9d_fc9a_4d80_9e32_728e097879ad from '@/components/wwObjects/wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad/src/wwElement.vue' 
import wwobject_086d46a0_9232_4df3_8119_7d3c8779cb1a from '@/components/wwObjects/wwobject-086d46a0-9232-4df3-8119-7d3c8779cb1a/src/wwElement.vue' 
import wwobject_b10e81a1_a2b7_4597_8625_60fe0b1722e1 from '@/components/wwObjects/wwobject-b10e81a1-a2b7-4597-8625-60fe0b1722e1/src/wwElement.vue' 
import wwobject_25edb26f_a365_4447_8de5_4964f9f7dc77 from '@/components/wwObjects/wwobject-25edb26f-a365-4447-8de5-4964f9f7dc77/src/wwElement.vue' 
import wwobject_700d9467_e29c_4129_b103_664e9d8611dd from '@/components/wwObjects/wwobject-700d9467-e29c-4129-b103-664e9d8611dd/src/wwElement.vue' 
import wwobject_b783dc65_d528_4f74_8c14_e27c934c39b1 from '@/components/wwObjects/wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1/src/wwElement.vue' 
import wwobject_6f8796b1_8273_498d_95fc_7013b7c63214 from '@/components/wwObjects/wwobject-6f8796b1-8273-498d-95fc-7013b7c63214/src/wwElement.vue' 
import wwobject_0489cac7_a094_4607_b9f0_bb793693d021 from '@/components/wwObjects/wwobject-0489cac7-a094-4607-b9f0-bb793693d021/src/wwElement.vue' 
import wwobject_70a53858_53ca_40a5_ad88_c1cd33b5cc9f from '@/components/wwObjects/wwobject-70a53858-53ca-40a5-ad88-c1cd33b5cc9f/src/wwElement.vue' 
import wwobject_aeb78b9a_6fb6_4c49_931d_faedcfad67ba from '@/components/wwObjects/wwobject-aeb78b9a-6fb6-4c49-931d-faedcfad67ba/src/wwElement.vue' 
import wwobject_21881619_a984_4847_81a9_922c3dbb5853 from '@/components/wwObjects/wwobject-21881619-a984-4847-81a9-922c3dbb5853/src/wwElement.vue' 
import wwobject_83d890fb_84f9_4386_b459_fb4be89a8e15 from '@/components/wwObjects/wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15/src/wwElement.vue' 
;

// eslint-disable-next-line no-undef
app.component('wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d', wwobject_fb3e0024_f017_4193_a6a1_bc2eed330d1d) 
app.component('wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad', wwobject_d7904e9d_fc9a_4d80_9e32_728e097879ad) 
app.component('wwobject-086d46a0-9232-4df3-8119-7d3c8779cb1a', wwobject_086d46a0_9232_4df3_8119_7d3c8779cb1a) 
app.component('wwobject-b10e81a1-a2b7-4597-8625-60fe0b1722e1', wwobject_b10e81a1_a2b7_4597_8625_60fe0b1722e1) 
app.component('wwobject-25edb26f-a365-4447-8de5-4964f9f7dc77', wwobject_25edb26f_a365_4447_8de5_4964f9f7dc77) 
app.component('wwobject-700d9467-e29c-4129-b103-664e9d8611dd', wwobject_700d9467_e29c_4129_b103_664e9d8611dd) 
app.component('wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1', wwobject_b783dc65_d528_4f74_8c14_e27c934c39b1) 
app.component('wwobject-6f8796b1-8273-498d-95fc-7013b7c63214', wwobject_6f8796b1_8273_498d_95fc_7013b7c63214) 
app.component('wwobject-0489cac7-a094-4607-b9f0-bb793693d021', wwobject_0489cac7_a094_4607_b9f0_bb793693d021) 
app.component('wwobject-70a53858-53ca-40a5-ad88-c1cd33b5cc9f', wwobject_70a53858_53ca_40a5_ad88_c1cd33b5cc9f) 
app.component('wwobject-aeb78b9a-6fb6-4c49-931d-faedcfad67ba', wwobject_aeb78b9a_6fb6_4c49_931d_faedcfad67ba) 
app.component('wwobject-21881619-a984-4847-81a9-922c3dbb5853', wwobject_21881619_a984_4847_81a9_922c3dbb5853) 
app.component('wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15', wwobject_83d890fb_84f9_4386_b459_fb4be89a8e15) 
;

// eslint-disable-next-line no-undef
import section_4d0e32dd_3174_4f10_ba60_08273cb1f755 from '@/components/sectionBases/section-4d0e32dd-3174-4f10-ba60-08273cb1f755/src/wwSection.vue' 
import section_99586bd3_2b15_4d6b_a025_6a50d07ca845 from '@/components/sectionBases/section-99586bd3-2b15-4d6b-a025-6a50d07ca845/src/wwSection.vue' 
;

// eslint-disable-next-line no-undef
app.component('section-4d0e32dd-3174-4f10-ba60-08273cb1f755', section_4d0e32dd_3174_4f10_ba60_08273cb1f755) 
app.component('section-99586bd3-2b15-4d6b-a025-6a50d07ca845', section_99586bd3_2b15_4d6b_a025_6a50d07ca845) 
;
