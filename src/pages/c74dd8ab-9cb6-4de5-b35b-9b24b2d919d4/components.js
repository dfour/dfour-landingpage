// eslint-disable-next-line no-unused-vars
import app from '@/_front/main.js';
import { defineAsyncComponent } from 'vue';


// eslint-disable-next-line no-undef
import wwobject_d7904e9d_fc9a_4d80_9e32_728e097879ad from '@/components/wwObjects/wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad/src/wwElement.vue' 
import wwobject_6f8796b1_8273_498d_95fc_7013b7c63214 from '@/components/wwObjects/wwobject-6f8796b1-8273-498d-95fc-7013b7c63214/src/wwElement.vue' 
import wwobject_700d9467_e29c_4129_b103_664e9d8611dd from '@/components/wwObjects/wwobject-700d9467-e29c-4129-b103-664e9d8611dd/src/wwElement.vue' 
import wwobject_83d890fb_84f9_4386_b459_fb4be89a8e15 from '@/components/wwObjects/wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15/src/wwElement.vue' 
import wwobject_6d692ca2_6cdc_4805_aa0c_211102f335d0 from '@/components/wwObjects/wwobject-6d692ca2-6cdc-4805-aa0c-211102f335d0/src/wwElement.vue' 
import wwobject_fb3e0024_f017_4193_a6a1_bc2eed330d1d from '@/components/wwObjects/wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d/src/wwElement.vue' 
import wwobject_b783dc65_d528_4f74_8c14_e27c934c39b1 from '@/components/wwObjects/wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1/src/wwElement.vue' 
import wwobject_21881619_a984_4847_81a9_922c3dbb5853 from '@/components/wwObjects/wwobject-21881619-a984-4847-81a9-922c3dbb5853/src/wwElement.vue' 
import wwobject_0489cac7_a094_4607_b9f0_bb793693d021 from '@/components/wwObjects/wwobject-0489cac7-a094-4607-b9f0-bb793693d021/src/wwElement.vue' 
;

// eslint-disable-next-line no-undef
app.component('wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad', wwobject_d7904e9d_fc9a_4d80_9e32_728e097879ad) 
app.component('wwobject-6f8796b1-8273-498d-95fc-7013b7c63214', wwobject_6f8796b1_8273_498d_95fc_7013b7c63214) 
app.component('wwobject-700d9467-e29c-4129-b103-664e9d8611dd', wwobject_700d9467_e29c_4129_b103_664e9d8611dd) 
app.component('wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15', wwobject_83d890fb_84f9_4386_b459_fb4be89a8e15) 
app.component('wwobject-6d692ca2-6cdc-4805-aa0c-211102f335d0', wwobject_6d692ca2_6cdc_4805_aa0c_211102f335d0) 
app.component('wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d', wwobject_fb3e0024_f017_4193_a6a1_bc2eed330d1d) 
app.component('wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1', wwobject_b783dc65_d528_4f74_8c14_e27c934c39b1) 
app.component('wwobject-21881619-a984-4847-81a9-922c3dbb5853', wwobject_21881619_a984_4847_81a9_922c3dbb5853) 
app.component('wwobject-0489cac7-a094-4607-b9f0-bb793693d021', wwobject_0489cac7_a094_4607_b9f0_bb793693d021) 
;

// eslint-disable-next-line no-undef
import section_99586bd3_2b15_4d6b_a025_6a50d07ca845 from '@/components/sectionBases/section-99586bd3-2b15-4d6b-a025-6a50d07ca845/src/wwSection.vue' 
;

// eslint-disable-next-line no-undef
app.component('section-99586bd3-2b15-4d6b-a025-6a50d07ca845', section_99586bd3_2b15_4d6b_a025_6a50d07ca845) 
;
