export default {
    editor: {
        label: {
            en: 'Absolute popup',
        },
    },
    properties: {
        absoluteReference: {
            label: {
                en: 'Position',
                fr: 'Position',
            },
            type: 'TextSelect',
            options: {
                options: [
                    { value: 'topLeft', label: { en: 'Top - Left' } },
                    { value: 'bottomLeft', label: { en: 'Bottom - Left' } },
                    { value: 'bottomRight', label: { en: 'Bottom - Right' } },
                    { value: 'topRight', label: { en: 'Top - Right' } },
                ],
            },
            responsive: true,
            defaultValue: 'bottomRight',
        },
        positionY: {
            type: 'Length',
            label: {
                en: 'Position Y',
                fr: 'Position Y',
            },
            options: {
                unitChoices: [
                    { value: 'px', label: 'px' },
                    { value: '%', label: '%' },
                ],
            },
            responsive: true,
            defaultValue: '20px',
        },
        positionX: {
            type: 'Length',
            label: {
                en: 'Position X',
                fr: 'Position X',
            },
            options: {
                unitChoices: [
                    { value: 'px', label: 'px' },
                    { value: '%', label: '%' },
                ],
            },
            responsive: true,
            defaultValue: '20px',
        },
        adjustForCenter: {
            type: 'OnOff',
            label: {
                en: 'Adjust centering',
                fr: 'Adjust centering',
            },
            responsive: true,
            defaultValue: false,
        },
        behavior: {
            label: {
                en: 'Behavior',
                fr: 'Comportement',
            },
            type: 'TextSelect',
            options: {
                options: [
                    { value: 'timeout', label: { en: 'Timeout' } },
                    { value: 'scroll', label: { en: 'Scroll' } },
                ],
            },
            section: 'settings',
            defaultValue: 'scroll',
        },
        reference: {
            hidden: content => content.behavior === 'timeout',
            label: {
                en: 'Reference',
                fr: 'Reference',
            },
            type: 'TextSelect',
            options: {
                options: [
                    { value: 'pixel', label: { en: 'Pixel' } },
                    { value: 'section', label: { en: 'Section' } },
                ],
            },
            section: 'settings',
            defaultValue: 'section',
        },
        timeoutOptions: {
            hidden: content => content.behavior !== 'timeout',
            type: 'Length',
            label: {
                en: 'Timeout',
                fr: 'Timeout',
            },
            options: {
                unitChoices: [{ value: 's', label: 's', min: 1, max: 1000 }],
            },
            section: 'settings',
            defaultValue: '4s',
        },
        sectionSelected: {
            hidden: content => content.reference === 'pixel' || content.behavior === 'timeout',
            label: {
                en: 'Appear at',
                fr: 'Appear at',
            },
            type: 'Section',
            section: 'settings',
        },
        scrollPixel: {
            hidden: content => content.reference === 'section' || content.behavior === 'timeout',
            type: 'Length',
            label: {
                en: 'Appear at',
                fr: 'Appear at',
            },
            options: {
                unitChoices: [{ value: 'px', label: 'px', min: 0 }],
            },
            defaultValue: '300xp',
            section: 'settings',
        },
        hide: {
            hidden: content => content.reference === 'section' || content.behavior === 'timeout',
            type: 'OnOff',
            label: {
                en: 'Hide on scroll back',
                fr: 'Hide on scroll back',
            },
            defaultValue: false,
            section: 'settings',
        },
        customCloseElement: {
            type: 'OnOff',
            label: {
                en: 'Custom close element',
                fr: 'Custom close element',
            },
            defaultValue: false,
        },
        icon: {
            hidden: true,
            defaultValue: { isWwObject: true, type: 'ww-icon' },
        },
        popupContent: {
            hidden: true,
            defaultValue: [],
        },
    },
};
