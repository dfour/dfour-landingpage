export default {
    isPrerenderHydration: true,
    /*=============================================m_ÔÔ_m=============================================\
        LANG
    \================================================================================================*/
    lang: 'en',
    /*=============================================m_ÔÔ_m=============================================\
        SCREEN SIZE
    \================================================================================================*/
    screenSize: null,

    //TODO : When comming from publisher, also inject in prerenderProcess.js
    screenSizes: {
        default: {
            order: 0,
            label: {
                en: 'Desktop',
                fr: 'Ordinateur',
            },
            breakPoint: null,
            icon: 'monitor',
            query: null,
            defaultWidth: 1100,
            defaultHeight: (1100 * 9) / 16,
            prerenderWidth: 1920,
        },
        tablet: {
            order: 1,
            label: {
                en: 'Tablet',
                fr: 'Tablette',
            },
            breakPoint: 991,
            icon: 'tablet',
            query: 'max-width: 991px',
            defaultWidth: 770,
            defaultHeight: (770 * 14) / 9,
            prerenderWidth: 991,
        },
        mobile: {
            order: 2,
            label: {
                en: 'Mobile',
                fr: 'Mobile',
            },
            breakPoint: 767,
            icon: 'mobile',
            query: 'max-width: 767px',
            defaultWidth: 400,
            defaultHeight: (400 * 13) / 9,
            prerenderWidth: 767,
        },
    },
    isScreenSizeActive: {},
    /*=============================================m_ÔÔ_m=============================================\
        COMPONENTS CONFIG
    \================================================================================================*/
    /* wwFront:start */
    // eslint-disable-next-line no-undef
    componentConfigs: {'plugin-29809245-a5ea-4687-af79-952998abab22': { ...require('@/components/plugins/plugin-29809245-a5ea-4687-af79-952998abab22/ww-config.js').default, name: 'plugin-29809245-a5ea-4687-af79-952998abab22' },'plugin-615d0fde-2084-4ac2-a2a4-5eb7686446ce': { ...require('@/components/plugins/plugin-615d0fde-2084-4ac2-a2a4-5eb7686446ce/ww-config.js').default, name: 'plugin-615d0fde-2084-4ac2-a2a4-5eb7686446ce' },'plugin-2bd1c688-31c5-443e-ae25-59aa5b6431fb': { ...require('@/components/plugins/plugin-2bd1c688-31c5-443e-ae25-59aa5b6431fb/ww-config.js').default, name: 'plugin-2bd1c688-31c5-443e-ae25-59aa5b6431fb' },'plugin-d8a4685d-3cff-4975-87f1-bc8ae8878260': { ...require('@/components/plugins/plugin-d8a4685d-3cff-4975-87f1-bc8ae8878260/ww-config.js').default, name: 'plugin-d8a4685d-3cff-4975-87f1-bc8ae8878260' },'wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad': { ...require('@/components/wwObjects/wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad/ww-config.js').default, name: 'wwobject-d7904e9d-fc9a-4d80-9e32-728e097879ad' },'wwobject-6f8796b1-8273-498d-95fc-7013b7c63214': { ...require('@/components/wwObjects/wwobject-6f8796b1-8273-498d-95fc-7013b7c63214/ww-config.js').default, name: 'wwobject-6f8796b1-8273-498d-95fc-7013b7c63214' },'wwobject-700d9467-e29c-4129-b103-664e9d8611dd': { ...require('@/components/wwObjects/wwobject-700d9467-e29c-4129-b103-664e9d8611dd/ww-config.js').default, name: 'wwobject-700d9467-e29c-4129-b103-664e9d8611dd' },'wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15': { ...require('@/components/wwObjects/wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15/ww-config.js').default, name: 'wwobject-83d890fb-84f9-4386-b459-fb4be89a8e15' },'wwobject-25edb26f-a365-4447-8de5-4964f9f7dc77': { ...require('@/components/wwObjects/wwobject-25edb26f-a365-4447-8de5-4964f9f7dc77/ww-config.js').default, name: 'wwobject-25edb26f-a365-4447-8de5-4964f9f7dc77' },'wwobject-aeb78b9a-6fb6-4c49-931d-faedcfad67ba': { ...require('@/components/wwObjects/wwobject-aeb78b9a-6fb6-4c49-931d-faedcfad67ba/ww-config.js').default, name: 'wwobject-aeb78b9a-6fb6-4c49-931d-faedcfad67ba' },'wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d': { ...require('@/components/wwObjects/wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d/ww-config.js').default, name: 'wwobject-fb3e0024-f017-4193-a6a1-bc2eed330d1d' },'wwobject-57831abf-83ad-49ad-ba97-3bd30b035710': { ...require('@/components/wwObjects/wwobject-57831abf-83ad-49ad-ba97-3bd30b035710/ww-config.js').default, name: 'wwobject-57831abf-83ad-49ad-ba97-3bd30b035710' },'wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1': { ...require('@/components/wwObjects/wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1/ww-config.js').default, name: 'wwobject-b783dc65-d528-4f74-8c14-e27c934c39b1' },'wwobject-21881619-a984-4847-81a9-922c3dbb5853': { ...require('@/components/wwObjects/wwobject-21881619-a984-4847-81a9-922c3dbb5853/ww-config.js').default, name: 'wwobject-21881619-a984-4847-81a9-922c3dbb5853' },'wwobject-0489cac7-a094-4607-b9f0-bb793693d021': { ...require('@/components/wwObjects/wwobject-0489cac7-a094-4607-b9f0-bb793693d021/ww-config.js').default, name: 'wwobject-0489cac7-a094-4607-b9f0-bb793693d021' },'wwobject-6dee3327-9afa-4b44-bade-bdb547cdb18b': { ...require('@/components/wwObjects/wwobject-6dee3327-9afa-4b44-bade-bdb547cdb18b/ww-config.js').default, name: 'wwobject-6dee3327-9afa-4b44-bade-bdb547cdb18b' },'section-99586bd3-2b15-4d6b-a025-6a50d07ca845': { ...require('@/components/sectionBases/section-99586bd3-2b15-4d6b-a025-6a50d07ca845/ww-config.js').default, name: 'section-99586bd3-2b15-4d6b-a025-6a50d07ca845' },'wwobject-6d692ca2-6cdc-4805-aa0c-211102f335d0': { ...require('@/components/wwObjects/wwobject-6d692ca2-6cdc-4805-aa0c-211102f335d0/ww-config.js').default, name: 'wwobject-6d692ca2-6cdc-4805-aa0c-211102f335d0' },'wwobject-aac1b1b8-e4e5-492f-a02b-7cc0866ba36e': { ...require('@/components/wwObjects/wwobject-aac1b1b8-e4e5-492f-a02b-7cc0866ba36e/ww-config.js').default, name: 'wwobject-aac1b1b8-e4e5-492f-a02b-7cc0866ba36e' },'section-fedde108-a9d5-40e0-b56d-52c4c9739e0e': { ...require('@/components/sectionBases/section-fedde108-a9d5-40e0-b56d-52c4c9739e0e/ww-config.js').default, name: 'section-fedde108-a9d5-40e0-b56d-52c4c9739e0e' },'wwobject-086d46a0-9232-4df3-8119-7d3c8779cb1a': { ...require('@/components/wwObjects/wwobject-086d46a0-9232-4df3-8119-7d3c8779cb1a/ww-config.js').default, name: 'wwobject-086d46a0-9232-4df3-8119-7d3c8779cb1a' },'wwobject-b10e81a1-a2b7-4597-8625-60fe0b1722e1': { ...require('@/components/wwObjects/wwobject-b10e81a1-a2b7-4597-8625-60fe0b1722e1/ww-config.js').default, name: 'wwobject-b10e81a1-a2b7-4597-8625-60fe0b1722e1' },'wwobject-70a53858-53ca-40a5-ad88-c1cd33b5cc9f': { ...require('@/components/wwObjects/wwobject-70a53858-53ca-40a5-ad88-c1cd33b5cc9f/ww-config.js').default, name: 'wwobject-70a53858-53ca-40a5-ad88-c1cd33b5cc9f' },'section-4d0e32dd-3174-4f10-ba60-08273cb1f755': { ...require('@/components/sectionBases/section-4d0e32dd-3174-4f10-ba60-08273cb1f755/ww-config.js').default, name: 'section-4d0e32dd-3174-4f10-ba60-08273cb1f755' },},
    /* wwFront:end */
    activeLinkPopup: null,
    elementStates: {},
};
