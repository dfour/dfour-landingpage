/* eslint-disable no-console */
const path = require('path');
const PrerenderSPAPlugin = require('@weweb/prerender-spa');
const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;
const fs = require('fs');

const PRERENDER_DISABLED = `${process.env.PRERENDER_DISABLED}` == 'true' ? true : false;
process.env.VUE_APP_PRERENDER_DISABLED = process.env.PRERENDER_DISABLED || false;
process.env.VUE_APP_VERSION = process.env.npm_package_version;
// eslint-disable-next-line no-undef
const allRoutes = ["/en/recbl5ximzcpyjank","/recbl5ximzcpyjank","/en/recfx8lg1hvmkqbfo","/recfx8lg1hvmkqbfo","/en/rechqyccxlbrfvt4s","/rechqyccxlbrfvt4s","/en/recsymbha1kqn4bml","/recsymbha1kqn4bml","/en/recmj3p2v6gdrg41d","/recmj3p2v6gdrg41d","/en/rec15ept2drphptjz","/rec15ept2drphptjz","/en/recksg0dklmpunc3r","/recksg0dklmpunc3r","/en/recaakweizohu2ts6","/recaakweizohu2ts6","/en/recglqibyopecgbo4","/recglqibyopecgbo4","/en/rec6zmiutmy7scbzu","/rec6zmiutmy7scbzu","/en/rectojzqlf42elqwe","/rectojzqlf42elqwe","/en/recp2sdszhjjon10l","/recp2sdszhjjon10l","/en/recjlldk5t9udxzi5","/recjlldk5t9udxzi5","/en/recd1ygbrvasslui6","/recd1ygbrvasslui6","/en/recg8912dgwf2qsnj","/recg8912dgwf2qsnj","/en/signup","/signup","/en/detail","/detail","/en/catalogue","/catalogue","/en/demo","/demo","/en","/"];

module.exports = function () {
    const options = {
        productionSourceMap: false,
        outputDir: './dist/',
        assetsDir: 'public',
        publicPath: '/',
    };

    options.pages = {
        index: {
            // entry for the page
            entry: 'src/_front/main.js',
            // the source template
            template: 'public/index_for_prerender.html',
            // output as dist/index.html
            filename: 'index_for_prerender.html',
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Index Page',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    };

    options.configureWebpack = config => {
        console.log('');
        console.log('BUILDING VERSION : ' + process.env.npm_package_version);
        console.log('MODE : ' + process.env.MODE);
        console.log('');

        config.module.rules.push({
            test: /\.?(jsx|tsx)(\?.*)?$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-react'],
                    plugins: ['@babel/transform-react-jsx'],
                },
            },
        });

        config.plugins.push(
            new PrerenderSPAPlugin({
                indexPath: path.join(__dirname, 'dist', 'index_for_prerender.html'),

                // Required - The path to the webpack-outputted app to prerender.
                staticDir: path.join(__dirname, 'dist'),

                // Optional - The path your rendered app should be output to.
                // (Defaults to staticDir.)
                outputDir: path.join(__dirname, 'dist'),

                // Optional - The location of index.html
                // indexPath: path.join(__dirname, 'dist', 'index.html'),

                // Required - Routes to render.
                // eslint-disable-next-line no-undef
                routes: process.env.ROUTES_TO_RENDER ? JSON.parse(process.env.ROUTES_TO_RENDER) : allRoutes,
                // routes: ['/', '/home', '/about', '/fr', '/fr/home', '/fr/about', '/en', '/en/home', '/en/about'],

                // Optional - Uses html-minifier (https://github.com/kangax/html-minifier)
                // To minify the resulting HTML.
                // Option reference: https://github.com/kangax/html-minifier#options-quick-reference
                minify: {
                    collapseBooleanAttributes: true,
                    // collapseWhitespace: true,
                    decodeEntities: true,
                    keepClosingSlash: true,
                    sortAttributes: true,
                },

                // Server configuration options.
                server: {
                    // Normally a free port is autodetected, but feel free to set this if needed.
                    // port: 8001
                },

                // The actual renderer to use. (Feel free to write your own)
                // Available renderers: https://github.com/Tribex/prerenderer/tree/master/renderers
                renderer: new Renderer({
                    // Optional - Wait to render until the specified event is dispatched on the document.
                    // eg, with `document.dispatchEvent(new Event('custom-render-trigger'))`
                    renderAfterDocumentEvent: 'ww-prerender-page',
                    // renderAfterTime: 200000,
                    maxConcurrentRoutes: 10,
                    skipThirdPartyRequests: true,
                    timeout: 0,
                    // headless: false,

                    // Optional - Any values you'd like your app to have access to via `window.injectProperty`.
                    inject: true,
                    injectProperty: '__WW_IS_PRERENDER__',

                    defaultViewport: {
                        width: 1920,
                        height: 1080,
                    },
                    allowedUrls: [
                        'https://cdn.weweb.io/',
                        'https://cdn.weweb-preprod.io/',
                        'https://cdn.weweb-staging.io/',
                        'https://weweb-v3.twic.pics/',
                        'https://dl.airtable.com/',
                        'https://cdn.weweb.app/',
                        'https://fonts.googleapis.com/',
                        'https://fonts.gstatic.com/',
                    ],
                }),

                postProcess(renderedRoute) {
                    //Save screenShot
                    fs.mkdirSync(path.join(__dirname, `dist${renderedRoute.route}/`), { recursive: true });
                    fs.writeFileSync(
                        path.join(__dirname, `dist${renderedRoute.route}/screen.png`),
                        renderedRoute.screenShot
                    );

                    //Add id=app
                    renderedRoute.html = renderedRoute.html.replace(
                        /<\/noscript>\s*<div(.*)class="website-wrapper"/gi,
                        '</noscript><div id="app" $1 class="website-wrapper"'
                    );

                    //Add async to all scripts
                    renderedRoute.html = renderedRoute.html.replace(/><\/script>/gi, ' async></script>');

                    //Extract styles and generate media files
                    if (!PRERENDER_DISABLED) {
                        const random = Math.round(Math.random() * 10000000000);
                        const screens = ['default', 'tablet', 'mobile'];
                        for (const screen of screens) {
                            let regexp = new RegExp(
                                `<style generated-css="${screen}" generated-media="(.*?)">(.*?)</style>`,
                                'gi'
                            );
                            const matches = regexp.exec(renderedRoute.html);

                            const media = matches[1];
                            const css = matches[2];

                            fs.mkdirSync(path.join(__dirname, `dist/public/css${renderedRoute.route}/`), {
                                recursive: true,
                            });
                            fs.writeFileSync(
                                path.join(__dirname, `dist/public/css${renderedRoute.route}/${screen}-${random}.css`),
                                css
                            );

                            renderedRoute.html = renderedRoute.html.replace(
                                regexp,
                                `<link generated-css="${screen}" href="public/css${renderedRoute.route}${
                                    renderedRoute.route.endsWith('/') ? '' : '/'
                                }${screen}-${random}.css" rel="stylesheet" media="${media}">`
                            );
                        }
                    }

                    //BASE TAG
                    let baseTag = {} || {};
                    if (Object.keys(baseTag).length !== 2) baseTag = { href: '/' };
                    renderedRoute.html = renderedRoute.html.replace(
                        /<head>/gi,
                        `<head><base href="${baseTag.href || '/'}" ${
                            baseTag.target ? `target="${baseTag.target}"` : ''
                        }/>`
                    );

                    // /public/
                    renderedRoute.html = renderedRoute.html.replace(/src="\/public\//gi, 'src="public/');
                    renderedRoute.html = renderedRoute.html.replace(/href="\/public\//gi, 'href="public/');

                    //PAGE SCRIPTS
                    const regex = /<(?:div|body)(?:.*)ww-page-id="([^"]*)"/gi;
                    const match = regex.exec(renderedRoute.html);

                    if (match && match[1]) {
                        const id = match[1];

                        // eslint-disable-next-line no-undef
                        const scripts = {"head":{"04a57aef-0ef9-427a-889e-0303a4c8b564_0":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_1":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_2":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_3":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_4":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_5":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_6":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_7":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_8":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_9":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_10":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_11":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_12":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_13":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","04a57aef-0ef9-427a-889e-0303a4c8b564_14":"<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n","cb3dea78-44ae-4247-8ff1-b645916568a3":"<script src=\"https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous\"></script>\n\n<script src=\"https://cdn.snipcart.com/scripts/2.0/snipcart.js\" data-api-key=\"OTIzMjAwMDEtYzBjNC00YTc1LTkzYjItZmU2ODQ2MjBiOTQzNjM3NjMxNDIxODA1MjM5ODAw\" id=\"snipcart\"></script>\n\n<link href=\"https://cdn.snipcart.com/themes/2.0/base/snipcart.min.css\" rel=\"stylesheet\" type=\"text/css\" />","78f321d0-21bd-432a-a4db-3506fe486511":"<script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id=\"typef_orm_share\", b=\"https://embed.typeform.com/\"; if(!gi.call(d,id)){ js=ce.call(d,\"script\"); js.id=id; js.src=b+\"embed.js\"; q=gt.call(d,\"script\")[0]; q.parentNode.insertBefore(js,q) } })() </script>","d8c99c90-76c6-4000-aef7-c623fe373798":"<script>window.pipedriveLeadboosterConfig = {base: 'leadbooster-chat.pipedrive.com',companyId: 10402534,playbookUuid: '055252b3-88fa-47f5-b53b-dbb2c2c69b19',version: 2};(function () {var w = window;if (w.LeadBooster) {console.warn('LeadBooster already exists');} else {w.LeadBooster = {q: [],on: function (n, h) {this.q.push({ t: 'o', n: n, h: h });},trigger: function (n) {this.q.push({ t: 't', n: n });},};}})();</script><script src=\"https://leadbooster-chat.pipedrive.com/assets/loader.js\" async></script>\n\n<script> (function(ss,ex){ window.ldfdr=window.ldfdr||function(){(ldfdr._q=ldfdr._q||[]).push([].slice.call(arguments));}; (function(d,s){ fs=d.getElementsByTagName(s)[0]; function ce(src){ var cs=d.createElement(s); cs.src=src; cs.async=1; fs.parentNode.insertBefore(cs,fs); }; ce('https://sc.lfeeder.com/lftracker_v1_'+ss+(ex?'_'+ex:'')+'.js'); })(document,'script'); })('lYNOR8x2wjN4WQJZ'); </script>"},"body":{"cb3dea78-44ae-4247-8ff1-b645916568a3":"<div hidden id=\"snipcart\" data-api-key=\"<api_key>\">\n   <item-line>\n       <li class=\"snipcart__item__line snipcart__box\">\n           <div class=\"snipcart__item__line__product\">\n               <div class=\"snipcart__item__line__header\">\n                   <h2 class=\"snipcart__item__line__header__title\">\n                       {{ item.name }}\n                   </h2>\n\n                   <item-quantity class=\"snipcart__item__line__quantity\" v-if=\"!adding\"></item-quantity>\n                   <div class=\"snipcart__item__line__header__actions\">\n                       <remove-item-action class=\"snipcart__button--icon\">\n                           <icon name=\"trash\" class=\"icon--red\"  alt=\"item.remove_item\"></icon>\n                       </remove-item-action>\n                   </div>\n               </div>\n           </div>\n       </li>\n   </item-line>\n</div>"},"globalBody":""};

                        if (scripts && scripts.head[id]) {
                            renderedRoute.html = renderedRoute.html.replace(/<\/head>/gi, scripts.head[id] + '</head>');
                        }
                        if (scripts && scripts.globalBody) {
                            renderedRoute.html = renderedRoute.html.replace(
                                /<\/body>/gi,
                                scripts.globalBody + '</body>'
                            );
                        }
                        if (scripts && scripts.body[id]) {
                            renderedRoute.html = renderedRoute.html.replace(/<\/body>/gi, scripts.body[id] + '</body>');
                        }
                    }

                    return renderedRoute;
                },
            })
        );
    };

    return options;
};
